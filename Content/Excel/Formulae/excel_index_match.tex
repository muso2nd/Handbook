\section{VLookup, Index and Match}
\label{sec:vlookup_index_and_match}
\subsection{Introduction}
\label{sub:introduction}
\textsc{VLookup} is a popular function for most Excel users although it has several disadvantages:
\begin{enumerate}
    \item Only left to right searchability. As described in table~\ref{tab:vsyn} \textsc{VLookup} only searches for the sought value in the first cell of the array of data. This means that either the spreadsheet has to be built from the start with that in mind or restructed to accomidate for this limitation of \textsc{VLookup}. 
    \item Broken by adding additional columns in the search area. \textsc{VLookup} is due to the formula input \emph{col\_index\_num} (see table~\ref{tab:vsyn}) not adjusting when new columns are added to the array. As spreadsheets usually aren't static this represents an additional workload that can be easily removed.
\end{enumerate}
Using the combination of the functions \textsc{Index} and \textsc{Match} solves those problems.
\subsection{\Gls{syntax} of Vlookup, Index and Match}
\label{sub:syntax_of_vlookup_index_and_match}
This subsection will first outline the \gls{syntax} of the functions \textsc{VLookup}, \textsc{Index} and \textsc{Match} and secondly display a more thorough description for each part of the function in table~\ref{tab:vsyn}, table~\ref{tab:isyn} and table~\ref{tab:msyn} respectivly.

\textsc{VLookup} \gls{syntax}\autocite{online_vlookup}:
\begin{verbatim}
   =VLOOKUP(lookup_value;table_array;col_index_num;range_lookup)
\end{verbatim}
\begin{table}[h]
    \caption[\textsc{Vlookup} function \gls{syntax}]{\textsc{VLookup} function syntax:} \begin{tabulary}{\textwidth}{ l J }
        \midrule
        lookup\_value (required): & The value you look for. The value you want to look up must be in the first column of the range of cells you specify in \emph{table\_array}. \\ 
        table\_array (required): & The range of cells in which the 	\textsc{VLookup} will search for the \emph{lookup\_value} and the return value. \\
        col\_index\_num (required): & The column number (starting with 1 for the left-most column of \emph{table\_array} that contains the return value.) \\
        range\_lookup (optional): & A logical value that specifies whether you want \textsc{Vlookup} to find an approximate or an exact match. \\
    \end{tabulary}
    \label{tab:vsyn}
\end{table}

\textsc{Index} \gls{syntax}\autocite{online_index}:
\begin{verbatim}
   =INDEX(array;row_num;column_num)
\end{verbatim}
\begin{table}[h]
    \caption[\textsc{Index} function \gls{syntax}]{\textsc{Index} function syntax:}
    \begin{tabulary}{\textwidth}{ l J }
        \midrule
        array (required): & A range of cells or an array constant. \\
        row\_num (required): & Selects the row in array from which to return a value. If \emph{row\_num} is omitted, \emph{column\_num} is required. \\
        column\_num (optional): & Selects the column in array from which to return a value. If \emph{column\_num} is omitted, \emph{row\_num} is required. \\
    \end{tabulary}
    \label{tab:isyn}
\end{table}

\textsc{Match} \gls{syntax}\autocite{online_match}:
\begin{verbatim}
   =MATCH(lookup_value;lookup_array;match_type)
\end{verbatim}
\begin{table}[h]
    \caption[\textsc{Match} function \gls{syntax}]{\textsc{Match} function syntax:}
    \begin{tabulary}{\textwidth}{ l J }
        \midrule
        lookup\_value (required): & The value that you want to match in \emph{lookup\_array}. \\
        lookup\_array (required): & The range of cells being searched. \\
        match\_type (optional): & 1 = finds the largest value that is less than or equal to \emph{lookup\_value}. The values in \emph{lookup\_array} must placed in ascending order. \\ 
                                & 0 = finds the first value that is equal to \emph{lookup\_value}. The values in \emph{lookup\_array} can be in any order. \\ 
                                &-1 = finds the smallest value that is greater or equal to \emph{lookup\_value}. The values in \emph{lookup\_array} must be placed in descending order.\\ 

    \end{tabulary}
    \label{tab:msyn}
\end{table}

\section{Index+Match}
\label{sec:index_match}
\subsection{Syntax of Index+Match}
\label{sub:syntax_of_index+match}
This is the usual \gls{syntax} for \textsc{Index+Match}: 
\begin{verbatim}
   =INDEX(array;MATCH(lookup_value;lookup_array;match_type))
\end{verbatim}
There are alternatives for the use cases of replacements of \textsc{HLookup} and a matrix lookup. This will be thematized in a later section. 
\begin{table}[h]
    \caption[\textsc{Index+Match} \gls{syntax}]{\textsc{Index+Match} syntax}
    \label{tab:imsyn}
    \begin{tabulary}{\textwidth}{ l J }
        \midrule
        array (required): & The column that contains the return value. Similar to \textsc{VLookup} \emph{col\_index\_num}. \\
        lookup\_value (required): & The value you look for. Equivalent to \textsc{VLookup} \emph{lookup\_value}. \\
        lookup\_array (required): & Column of cells that will be searched for the \emph{lookup\_value}.  \\
        match\_type (required): & See table~\ref{tab:msyn}. The usual value is 0 to find the exact match for the \emph{lookup\_value}. \\
    \end{tabulary}
\end{table}

\subsection{Usage with examples}
\label{sub:usage_with_examples}

The following figure~\ref{fig:im_bas_ex} shows an example of a basic spreadsheet using \textsc{Index+Match}. 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{images/indexmatch_basic_syntax_example.png}
    \caption{Basic example of \textsc{Index+Match}}
    \label{fig:im_bas_ex}
\end{figure}

Table~\ref{tab:im_bas_ex_comp} displays the different parts of the function and gives a brief description of each.

\begin{myverbbox}{\voneone}
C7:C13
\end{myverbbox}
\begin{myverbbox}{\vonetwo}
B3    
\end{myverbbox}
\begin{myverbbox}{\vonethree}
B7:B13 
\end{myverbbox}
\begin{myverbbox}{\vonefour}
0    
\end{myverbbox}
\begin{table}[h]
    \centering
    \caption{Components of the \textsc{Index+Match} example}
    \label{tab:im_bas_ex_comp}
    \begin{tabulary}{\textwidth}{ l J}
        \midrule
        \voneone & The column with the listing of the products. Depending on the value in \vonetwo~it will select the fitting product name. \\
        \vonetwo & The ID of the product we are looking for. \\
        \vonethree & The list of product IDs. \\
        \vonefour & Only displaying exact matches of \vonetwo. \\
    \end{tabulary}
\end{table}

Using this combination of functions \textsc{VLookup} can be emulated which gives the same features but also has some improvements over the original as previously stated. 

In the introduction it was stated that using \textsc{Index+Match} over \textsc{VLookup} has some advantages. The next examples (figure~\ref{fig:im_bas_rightindex} and~\ref{fig:im_bas_addcolumn}) will illustrate this. 

One of the big weaknesses of \textsc{VLookup} is only being able to have the \gls{uid} on the left side i.e. the first column of \emph{table\_array}. This decreases the flexibility how the data of a spreadsheet can be arranged and might force a restructure of it if it doesn't fit this restriction. 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{images/indexmatch_basic_rightindex.png}
    \caption{\textsc{Index+Match} with the index right.}
    \label{fig:im_bas_rightindex}
\end{figure}

As can be seen in figure~\ref{fig:im_bas_rightindex} using \textsc{Index+Match} the \gls{uid} can be anywhere in the table. 

Spreadsheets are usually evolving over the years of adding, removing and reformatting data therefore having a function that is not adjusting to the changes automatically but has to be changed manually can increase the workload and possibilities of errors. 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{images/indexmatch_basic_addcolumn.png}
    \caption{\textsc{Index+Match} after adding a column.}
    \label{fig:im_bas_addcolumn}
\end{figure} 

While \textsc{VLookup} usually has problems dealing with columns added to the \emph{table\_array} due to the static nature of the \emph{col\_index\_num}. Using \textsc{Index+Match} automatically updates its formula with every column added and therefore decreases the maintenance needed.

\subsection{Index+Match with horizontal search}
\label{sub:index_match_with_horizontal_search}
In the previous section it was shown that \textsc{Index+Match} can replace \textsc{VLookup} and improve on it. By adjusting the \gls{syntax} minimally it can also be used as a replacement for \textsc{HLookup} and use a row instead of a column as a reference for its search. 

\begin{verbatim}
   =INDEX(array;;MATCH(lookup_value;lookup_array;match_type)) 
\end{verbatim}

By omitting the second parameter of the \textsc{Index} formula it switches from searching the columns to the rows as can be seen in \fref{tab:isyn}

\begin{table}[h]
    \centering
    \caption{\textsc{Index+Match} with horizontal search \gls{syntax}}
    \label{tab:im_horizontal_syntax}
    \begin{tabulary}{\textwidth}{l J}
        \midrule
        array (required): & The row that contains the return value. \\
        lookup\_value (required): & The value you look for.\\
        lookup\_array (required): & Row of cells that will be searched for the \emph{lookup\_value}.\\
        match\_type (required): & See \fref{tab:msyn}. Should be set to 0 to get the exact value.\\
    \end{tabulary}
\end{table}
The follow example shows a use-case of this variation of \textsc{Index+Match}. In this the amount of sales for a previously selected product in a specific month is searched for. 
\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{images/indexmatch_horizontal_example.png}
    \caption{Example of \textsc{Index+Match} with horizontal search.}
    \label{fig:im_horizontal_example}
\end{figure}

\subsection{Index+Match+Match}
\label{sub:index_match_match}
In the previous sections it was shown how to replace both \textsc{VLookup} and \textsc{HLookup} with \textsc{Index+Match}. By combining both variation of the function combination a matrix search can be implemented. 

The resulting \gls{syntax} will look like this:

\begin{verbatim}
   =INDEX(array;MATCH(lookup_value;lookup_array;match_type);
    MATCH(lookup_value;lookup_array;match_type)) 
\end{verbatim}

As can be seen in the formula two \gls{uid} are needed which are used as ``coordinates'' to navigate to the desired return value within the defined array. 

\begin{table}[h]
    \centering
    \caption{\textsc{Index+Match+Match} \gls{syntax}}
    \label{tab:im_match_syntax}
    \begin{tabulary}{\textwidth}{ l l J }
        \midrule
        \rowcolor{gray!25}
        \textsc{Index} &   \multicolumn{2}{l}{}   \\
                       & array (required): & The array that contains the return value. \\
        \rowcolor{gray!25}
        \textsc{Match} & \multicolumn{2}{l}{This \textsc{Match} deals with the search for the \gls{uid} in the columns.} \\
                       & lookup\_value (required): & The \gls{uid} which should be looked for in the column \emph{lookup\_array}. \\
                       & lookup\_array (required): & Column of cells that will be searched for the \emph{lookup\_value}.\\
                       & match\_type (required): & Should be set to 0 to get the exact match. For more information refer to \fref{tab:msyn}. \\ 
        \rowcolor{gray!25}
        \textsc{Match} & \multicolumn{2}{l}{This \textsc{Match} deals with the search for the \gls{uid} in the rows.} \\
                       & lookup\_value (required): & The \gls{uid} which should be looked for in the row \emph{lookup\_array}. \\
                       & lookup\_array (required): & Row of cells that will be searched for the \emph{lookup\_value}.\\
                       & match\_type (required) & Should be set to 0 to get the exact match. For more information refer to \fref{tab:msyn}. \\
    \end{tabulary}
\end{table}

The increase in functionality comes with a raise in complexity in the formula itself as is apparent in the table~\ref{tab:im_match_syntax}. An example where the increased functionality is displayed can be seen in figure~\ref{fig:im_match_example}. 

Here it is assumed that spreadsheet represents a sales table with multiple products over several months. Using the \textsc{Index+Match+Match} formula the user is able to find the amount of sales in a specific month for a specific product. 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{images/indexmatch_match_example.png}
    \caption{Example of \textsc{Index+Match+Match}}
    \label{fig:im_match_example}
\end{figure}
